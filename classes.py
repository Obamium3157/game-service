import random
import time


class Account:
    def __init__(self):
        self.money = 99999
        self.lootboxes = 3
        self.user_has_assasin = False
        self.user_has_pig = False
        self.message = ''

    def open_lootbox(self):
        chance = random.randint(1, 7)

        print("Поздравляем! Вы выбили:")

        if chance == 1:
            self.message = "Коровья лепешка!"
            print(self.message)

        if chance == 2:
            self.message = "Кусочек кала!"
            print(self.message)

        if chance == 3 and self.user_has_assasin:
            self.money += 15
            self.message = "У вас уже есть класс Ассасин! +15$"
            print(self.message)

        if chance == 3 and not self.user_has_assasin:
            self.user_has_assasin = True
            self.message = "Класс ассасин!"
            print(self.message)

        if chance == 4:
            self.message = "Конский навоз!"
            print(self.message)

        if chance == 5:
            self.money += 100
            self.message = "100$"
            print(self.message)

        if chance == 6 and self.user_has_pig:
            self.money -= 5
            time.sleep(3)
            self.message = "Свинья украла у вас 5$!"
            print(self.message)

        if chance == 6 and not self.user_has_pig:
            self.message = 'Вы нашли свинью!'
            print(self.message)
            time.sleep(2)
            self.message = "Свинья убежала!"
            print(self.message)
                
        if chance == 7:
            self.lootboxes += 1
            self.money += 100000
            self.message = 'Удача на вашей стороне! Вы получаете лутбокс и 100000$!!!'
            print(self.message)

    def check_command(self, command):
        if command == "/open" and self.lootboxes > 0:
            self.open_lootbox()
            self.lootboxes -= 1

        if command == "/open" and self.lootboxes <= 0:
            print("Нет лутбоктов!")

        if command == "/getmoney":
            self.get_some_money()

        if command == "/buy" and self.money < 100000:
            self.message = "Недостаточно денег!"
            print(self.message)

        if command == "/buy" and self.money >= 100000:
            self.money -= 100000
            self.lootboxes += 1
            self.message = "Вы купили лутбокс!"
            print(self.message)

        if command == "/buy 10" and self.money >= 1000000:
            self.money -= 1000000
            self.lootboxes += 10
            print("Вы купили 10 лутбоксов!")

        if command == "/viewprice":
            self.message = "Стоимость одного лутбокса - 100000$"
            print(self.message)

        if command == "/sell" and self.user_has_assasin:
            self.user_has_assasin = False
            self.money += 1000000
            self.message = "Вы продали ассасина! +1000000$"
            print(self.message)

        if command == "/help":
            self.message = "/open - открыть лутбокс\n",\
                           "/print - посмотреть инвентарь\n",\
                           "/getmoney - заработать деньги\n",\
                           "/buy - купить лутбокс\n",\
                           "/buy 10 - купить 10 лутбоксов\n",\
                           "/viewprice - стоимость лутбокса\n",\
                           "/sell - продать ассасина за 1000000$"
            print(self.message)

    def get_some_money(self):
        self.message = "Зарабатываем деньги"
        print(self.message)
        for i in range(0, 3):
            time.sleep(1)
        self.money += 1

        self.message = "Вы заработали немного денег и получили анальную боль"
        print(self.message)


