from classes import *
from tkinter import *
from tkinter import messagebox


a = Account()


def set_money_title():
    money_title.set("Деньги: {}$".format(a.money))


def set_lootboxes_title():
    lootboxes_title.set("Лутбоксы: {}".format(a.lootboxes))


def set_assasin_title():
    if a.user_has_assasin:
        user_has_assasin_title.set("У вас есть класс Ассасин!")
    else:
        user_has_assasin_title.set("У вас еще нет класса Ассасин!")


def read():
    command = command_entry_text.get()
    a.check_command(command)
    command_entry_text.set('')
    set_money_title()
    set_lootboxes_title()
    set_assasin_title()
    send_message()


def send_message():
    messagebox.showinfo("Поздравляем! Вы выбили:", a.message)


root = Tk()
root.title('Современная игра-сервис')
root.geometry("700x150")


# Main labels
money_title = StringVar()
set_money_title()
money = Label(textvariable=money_title, font="16")
money.grid(row=0, column=0)

lootboxes_title = StringVar()
set_lootboxes_title()
lootboxes = Label(textvariable=lootboxes_title, font="16")
lootboxes.grid(row=0, column=1)

user_has_assasin_title = StringVar()
set_assasin_title()
user_has_assasin = Label(textvariable=user_has_assasin_title, font="16")
user_has_assasin.grid(row=0, column=2)

help_title = Label(text='Наберите /help для помощи', font="16")
help_title.grid(row=0, column=3)


entry_label = Label(text='Поле для ввода команд', font="25")
entry_label.grid(row=1, column=2)

command_entry_text = StringVar()
command_entry = Entry(textvariable=command_entry_text)
command_entry.grid(row=2, column=2)


# Enter-a-command button
enter_a_command_button = Button(text="Отправить команду", command=read)
enter_a_command_button.grid(row=3, column=2)

root.mainloop()
